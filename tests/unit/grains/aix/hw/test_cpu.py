from dict_tools import data
import pytest
import mock

LSATTR_DATA = """
SW_dist_intr    false                                Enable SW distribution of interrupts                True
autorestart     true                                 Automatically REBOOT OS after a crash               True
boottype        disk                                 N/A                                                 False
capacity_inc    0.01                                 Processor capacity increment                        False
capped          false                                Partition is capped                                 False
chown_restrict  true                                 Chown Restriction Mode                              True
clouddev        0                                    Recreate ODM devices on next boot                   True
conslogin       enable                               System Console Login                                False
cpuguard        enable                               CPU Guard                                           True
dedicated       false                                Partition is dedicated                              False
enhanced_RBAC   true                                 Enhanced RBAC Mode                                  True
ent_capacity    0.40                                 Entitled processor capacity                         False
frequency       1600000000                           System Bus Frequency                                False
fullcore        false                                Enable full CORE dump                               True
fwversion       IBM,FW830.00 (SV830_048)             Firmware version and revision levels                False
ghostdev        0                                    Recreate ODM devices on system change / modify PVID True
id_to_partition 0X800013692AF0000E                   Partition ID                                        False
id_to_system    0X800013692AF00000                   System ID                                           False
iostat          false                                Continuously maintain DISK I/O history              True
keylock         normal                               State of system keylock at boot time                False
log_pg_dealloc  true                                 Log predictive memory page deallocation events      True
max_capacity    0.40                                 Maximum potential processor capacity                False
max_logname     9                                    Maximum login name length at boot time              True
maxbuf          20                                   Maximum number of pages in block I/O BUFFER CACHE   True
maxmbuf         0                                    Maximum Kbytes of real memory allowed for MBUFS     True
maxpout         8193                                 HIGH water mark for pending write I/Os per file     True
maxuproc        128                                  Maximum number of PROCESSES allowed per user        True
min_capacity    0.10                                 Minimum potential processor capacity                False
minpout         4096                                 LOW water mark for pending write I/Os per file      True
modelname       IBM,8408-E8E                         Machine name                                        False
ncargs          256                                  ARG/ENV list size in 4K byte blocks                 True
nfs4_acl_compat secure                               NFS4 ACL Compatibility Mode                         True
ngroups_allowed 128                                  Number of Groups Allowed                            True
os_uuid         01e487b7-faca-48b5-b89d-588f10aa63e4 N/A                                                 True
pmuaccess       all                                  PMU access control                                  True
pre430core      false                                Use pre-430 style CORE dump                         True
pre520tune      disable                              Pre-520 tuning compatibility mode                   True
realmem         4194304                              Amount of usable physical memory in Kbytes          False
rtasversion     1                                    Open Firmware RTAS version                          False
sed_config      select                               Stack Execution Disable (SED) Mode                  True
systemid        IBM,0221C490V                        Hardware system identifier                          False
variable_weight 128                                  Variable processor capacity weight                  False
"""

PRTCONF_DATA = """
System Model: IBM,8408-E8E
Machine Serial Number: 21CFFFF
Processor Type: PowerPC_POWER8
Processor Implementation Mode: POWER 8
Processor Version: PV_8_Compat
Number Of Processors: 2
Processor Clock Speed: 3359 MHz
CPU Type: 64-bit
Kernel Type: 64-bit
LPAR Info: 14 l490vp012
Memory Size: 4096 MB
Good Memory Size: 4096 MB
Platform Firmware level: SV830_048
Firmware Version: IBM,FW830.00 (SV830_048)
Console Login: enable
Auto Restart: true
Full Core: false
NX Crypto Acceleration: Capable and Enabled

en1
Network Information
        Host Name: l490vp012_pub
        IP Address: 10.153.33.12
        Sub Netmask:
        Gateway: 172.29.128.13
        Name Server: 10.153.50.201
        Domain Name: vlp.com

Paging Space Information
        Total Paging Space: 2688MB
        Percent Used: 0%

Volume Groups Information
==============================================================================
Active VGs
==============================================================================
rootvg:
PV_NAME           PV STATE          TOTAL PPs   FREE PPs    FREE DISTRIBUTION
hdisk14           active            279         78          22..00..00..00..56
==============================================================================

INSTALLED RESOURCE LIST

The following resources are installed on the machine.
+/- = Added or deleted from Resource List.
*   = Diagnostic support not available.

  Model Architecture: chrp
  Model Implementation: Multiple Processor, PCI bus

+ sys0                                                             System Object
+ sysplanar0                                                       System Planar
* vio0                                                             Virtual I/O Bus
* ent1             U8408.E8E.21C490V-V14-C13-T1                    Virtual I/O Ethernet Adapter (l-lan)
* ent0             U8408.E8E.21C490V-V14-C12-T1                    Virtual I/O Ethernet Adapter (l-lan)
* vscsi3           U8408.E8E.21C490V-V14-C23-T1                    Virtual SCSI Client Adapter
* vscsi2           U8408.E8E.21C490V-V14-C22-T1                    Virtual SCSI Client Adapter
* hdisk14          U8408.E8E.21C490V-V14-C22-T1-L8100000000000000  Virtual SCSI Disk Drive
* vsa0             U8408.E8E.21C490V-V14-C0                        LPAR Virtual Serial Adapter
* vty0             U8408.E8E.21C490V-V14-C0-L0                     Asynchronous Terminal
+ L2cache0                                                         L2 Cache
+ mem0                                                             Memory
+ proc0                                                            Processor
+ proc8                                                            Processor
"""


@pytest.mark.asyncio
async def test_load_cpuinfo(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": PRTCONF_DATA})

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.aix.hw.cpu.load_cpuinfo = hub.grains.aix.hw.cpu.load_cpuinfo
        await mock_hub.grains.aix.hw.cpu.load_cpuinfo()

    assert mock_hub.grains.GRAINS.cpuarch == "PowerPC_POWER8"
    assert mock_hub.grains.GRAINS.cpu_model == "POWER 8"
    assert mock_hub.grains.GRAINS.num_cpus == 2


@pytest.mark.asyncio
async def test_load_cpu_flags(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": LSATTR_DATA})

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.aix.hw.cpu.load_cpu_flags = hub.grains.aix.hw.cpu.load_cpu_flags
        await mock_hub.grains.aix.hw.cpu.load_cpu_flags()

    assert mock_hub.grains.GRAINS.cpu_flags == (
        "SW_dist_intr",
        "autorestart",
        "chown_restrict",
        "clouddev",
        "cpuguard",
        "enhanced_RBAC",
        "fullcore",
        "ghostdev",
        "iostat",
        "log_pg_dealloc",
        "max_logname",
        "maxbuf",
        "maxmbuf",
        "maxpout",
        "maxuproc",
        "minpout",
        "ncargs",
        "nfs4_acl_compat",
        "ngroups_allowed",
        "os_uuid",
        "pmuaccess",
        "pre430core",
        "pre520tune",
        "sed_config",
    )


@pytest.mark.asyncio
async def test_load_hardware_virtualization(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": "1"})

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.aix.hw.cpu.load_hardware_virtualization = (
            hub.grains.aix.hw.cpu.load_hardware_virtualization
        )
        await mock_hub.grains.aix.hw.cpu.load_hardware_virtualization()

    assert mock_hub.grains.GRAINS.hardware_virtualization is True
