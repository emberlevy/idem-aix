from dict_tools import data
import pytest
import mock

LSDEV_DATA = """
hdisk0  Defined    Virtual SCSI Disk Drive
hdisk1  Defined    Virtual SCSI Disk Drive
hdisk2  Defined    Virtual SCSI Disk Drive
hdisk3  Defined    Virtual SCSI Disk Drive
hdisk4  Defined    Virtual SCSI Disk Drive
hdisk5  Defined    Virtual SCSI Disk Drive
hdisk6  Defined    Virtual SCSI Disk Drive
hdisk7  Defined    Virtual SCSI Disk Drive
hdisk8  Defined    Virtual SCSI Disk Drive
hdisk9  Available  Virtual SCSI Disk Drive
hdisk10 Available  Virtual SCSI Disk Drive
hdisk11 Available  Virtual SCSI Disk Drive
hdisk12 Available  Virtual SCSI Disk Drive
hdisk13 Available  Virtual SCSI Disk Drive
hdisk14 Available  Virtual SCSI Disk Drive
"""


@pytest.mark.asyncio
async def test_load_disks(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": LSDEV_DATA})

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.aix.hw.disks.load_disks = hub.grains.aix.hw.disks.load_disks
        await mock_hub.grains.aix.hw.disks.load_disks()

    assert mock_hub.grains.GRAINS.disks == (
        "hdisk10",
        "hdisk11",
        "hdisk12",
        "hdisk13",
        "hdisk14",
        "hdisk9",
    )
    # assert mock_hub.grains.GRAINS.SSDs == ()
