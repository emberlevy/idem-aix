from dict_tools import data
import pytest


LSATTR_DATA = """
disc_filename  /etc/iscsi/targets            Configuration file                            False
disc_policy    file                          Discovery Policy                              True
initiator_name iqn.l50app020.hostid.0a991214 iSCSI Initiator Name                          True
isns_srvnames  auto                          iSNS Servers IP Addresses                     True
isns_srvports                                iSNS Servers Port Numbers                     True
max_targets    16                            Maximum Targets Allowed                       True
num_cmd_elems  200                           Maximum number of commands to queue to driver True
"""


@pytest.mark.asyncio
async def test_load_iqn(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": LSATTR_DATA})

    mock_hub.grains.aix.hw.iscsi.load_iqn = hub.grains.aix.hw.iscsi.load_iqn

    await mock_hub.grains.aix.hw.iscsi.load_iqn()

    assert mock_hub.grains.GRAINS.iscsi_iqn == ("iqn.l50app020.hostid.0a991214",)
