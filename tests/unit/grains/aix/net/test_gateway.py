from dict_tools import data
import pytest

NETSTAT4_DATA = """
Routing tables
Destination        Gateway           Flags   Refs     Use  If   Exp  Groups

Route Tree for Protocol Family 2 (Internet):
default            172.29.128.13     UG        2    429421 en0      -      -
10.153.0.0         10.153.33.12      UHSb      0         0 en1      -      -   =>
10.153/16          10.153.33.12      U         7      9741 en1      -      -
10.153.33.12       loopback          UGHS      0         1 lo0      -      -
10.153.255.255     10.153.33.12      UHSb      0         1 en1      -      -
127/8              loopback          U        11    312835 lo0      -      -
172.29.128.0       l490vp012_pub     UHSb      0         0 en0      -      -   =>
172.29.128/18      l490vp012_pub     U         1         0 en0      -      -
l490vp012_pub      loopback          UGHS      0         2 lo0      -      -
172.29.191.255     l490vp012_pub     UHSb      0         0 en0      -      -
192.168.1/24       10.153.1.99       UGS       0     66490 en1      -      -
"""

NETSTAT6_DATA = """
Routing tables
Destination        Gateway           Flags   Refs     Use  If   Exp  Groups

Route Tree for Protocol Family 24 (Internet v6):
::1%1              ::1%1             UH        0         0 lo0      -      -
"""


@pytest.mark.asyncio
async def test_load_gateway(mock_hub, hub):
    mock_hub.exec.cmd.run.side_effect = [
        data.NamespaceDict({"stdout": NETSTAT4_DATA}),
        data.NamespaceDict({"stdout": NETSTAT6_DATA}),
    ]

    mock_hub.grains.aix.net.gateway.load_default_gateway = (
        hub.grains.aix.net.gateway.load_default_gateway
    )

    await mock_hub.grains.aix.net.gateway.load_default_gateway()

    assert mock_hub.grains.GRAINS.ip_gw is True
    assert mock_hub.grains.GRAINS.ip4_gw == "172.29.128.13"
    assert mock_hub.grains.GRAINS.ip6_gw is False
