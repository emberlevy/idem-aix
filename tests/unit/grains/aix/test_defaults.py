import pytest


@pytest.mark.asyncio
async def test_load_defaults(mock_hub, hub):
    # Test hard coded grains
    assert mock_hub.grains.GRAINS.os_family == mock_hub.grains.GRAINS.os == "AIX"
    assert (
        mock_hub.grains.GRAINS.osmanufacturer
        == "International Business Machines Corporation"
    )
    assert mock_hub.grains.GRAINS.ps == "/usr/bin/ps auxww"
